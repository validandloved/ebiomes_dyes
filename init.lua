--[[ 
-- List of all dyes
{"white", "grey", "dark_grey", "black", 
"violet", "blue", "cyan", "dark_green", 
"green", "yellow", "brown", "orange",
"red", "magenta", "pink",}
]]--

-- Save each flower with it's dye colour
local ebiomes_dyelist = {
    {"black_iris", "black"},
    {"cladanthus", "white"},
    {"savory", "white"},
    {"staehelina", "magenta"},
    {"buttercup_persian", "red"},
    {"chrysanthemum_yellow", "yellow"},
    {"marigold", "yellow"},
    {"sundew", "pink"},
    {"blue_allium", "blue"},
    {"blue_allium_purple", "violet"},
    {"altai_tulip", "yellow"},
    {"russian_iris", "blue"},
    {"siberian_lily", "orange"},
    {"mountain_lily", "blue"},
    {"larkspur", "violet"},
    {"marsh_stitchwort", "white"},
}

-- Loop through that list and add the modnames then crafting
for _, row in ipairs(ebiomes_dyelist) do
	local flower = "ebiomes:"..row[1] -- "ebiomes:<flower>"
	local dye = "dye:"..row[2] -- "dye:<color>"
    dye = dye .. " 4" -- 1 flower = 4 dye     

	minetest.register_craft({
        type = "shapeless",
        output = dye, -- "dye:<color> 4"
        recipe = {flower} -- {"ebiomes:<flower>"}
    })
end