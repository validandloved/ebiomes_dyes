# Minetest: Dyes for Extra Biomes (ebiomes)

##### [this modname: `ebiomes_dyes`]

This mod adds the recipes for crafting dyes for all the flowers included in [`ebiomes`](https://content.minetest.net/packages/CowboyLv/ebiomes/). It gives 4 dyes per flower, just like the default ones.

On ContentDB [here](https://content.minetest.net/packages/Skivling/ebiomes_dyes).

##### Reason for making this:

[Extra Biomes](https://github.com/CowboyLva/ebiomes) includes lots of flowers, but no recipes for turning them into dyes. This is very annoying because if someone wants to get dyes, and they live in a biome from `ebiomes`, they will have to travel far to find a different biome to get a flower with the correct colour dye. With this mod, the flowers found in `ebiomes` can be crafted into dyes.

##### How it works:

If you want to know the specific colour for each flower, go in `init.lua` and look at `ebiomes_dyelist`. Each pair next to each other are the flower to dye colour.

The craft registering is handled by a loop function registering each pair in that list. 

This is all accomplished in only **40 lines**, including the list of dye colours at the top.

##### License:

by Skivling (MIT License) - see `LICENCE.txt`. 